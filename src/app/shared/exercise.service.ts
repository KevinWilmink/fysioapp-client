import { Injectable } from '@angular/core';
import {Exercise} from './exercise.model';
import {Subject} from 'rxjs/Subject';

// currently only used in the exercises-overview

@Injectable()
export class ExerciseService {

  exercises: Exercise[];

  exercisesChanged: Subject<Exercise[]> = new Subject<Exercise[]>();

  constructor() { }


  setExercises(exercises: Exercise[]) {
    this.exercises = exercises;
    this.exercisesChanged.next(this.exercises.slice());
  }
}
