import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Exercise} from '../exercise.model';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ExerciseService} from '../exercise.service';
import {DataStorageService} from '../data-storage.service';
import {Subscription} from 'rxjs/Subscription';
import {CategoriesService} from '../../overview/categories/categories.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-exercise',
  templateUrl: './exercise.component.html',
  styleUrls: ['./exercise.component.css']
})
export class ExerciseComponent implements OnInit, OnDestroy {

  @Input() exercise: Exercise  = new Exercise('', '', '', '', '', '', '', {});
  exerciseSub: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private exerciseService: ExerciseService,
              private dataStorage: DataStorageService,
              private categoriesService: CategoriesService,
              private router: Router,
              private domSanitizer: DomSanitizer) { }

  ngOnInit() {
    this.exerciseSub = this.activatedRoute.params.subscribe(
      (params: Params) => {
        // this.exercise = this.exerciseService.getExercise(params['id']);
        const exerciseId = params['id'];
        this.dataStorage.getExercise(exerciseId).subscribe(
          (exercise: Exercise) => {
            this.exercise = exercise;
            console.log(this.exercise);
          }
        );
      }
    );
  }

  getVideoUrl() { // TODO: doesn't seem to work
    // const videoUrlResult = 'https://www.youtube.com/v/' + this.exercise.youtubeVideoId;
    const videoUrlResult = 'https://www.youtube.com/embed/' + this.exercise.youtubeVideoId;
    const safeUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(videoUrlResult);
    return safeUrl;
  }

  ngOnDestroy() {
    this.exerciseSub.unsubscribe();
  }


  backToLastCategoryLoaded() {
    const url = 'categories/' + this.categoriesService.categoryLastLoadedId;
    console.log(url);
    this.router.navigate([url]);
  }
}
