import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Category} from '../overview/categories/category.model';
import {map, window} from 'rxjs/operators';
import {CategoriesService} from '../overview/categories/categories.service';
import {Exercise} from './exercise.model';
import {ExerciseService} from './exercise.service';
import {Observable} from 'rxjs/Observable';

import { environment } from '../../environments/environment';
import {AuthService} from '../auth/auth.service';
import {Organisation} from './organisations/organisation.model';
import {OrganisationsService} from './organisations/organisations.service';

@Injectable()
export class DataStorageService {

  // becarefully with organisationService and organisationsService
  constructor(private httpClient: HttpClient, private categoryService: CategoriesService, private exerciseService: ExerciseService,
              private authService: AuthService, private organisationsService: OrganisationsService) {
  }


  // does also provide info over the users in it
  loadOrganisationTherapist(therapistId) {
    console.log('load therapist: ' + therapistId);
    this.httpClient.get<Organisation>(environment.backendServer + '/organisations/' + therapistId + '/therapist',
      {observe: 'body', responseType: 'json'}).subscribe(
      (org: Organisation) => {
        this.organisationsService.setOrganisation(org);
      }
    );
  }


  loadOrganisations() {
    this.httpClient.get<Organisation[]>(environment.backendServer + '/organisations',
      {observe: 'body', responseType: 'json'}).pipe(map(
      (organisations) => {
        console.log(organisations);
        return organisations;
      }
    )).subscribe(
      (organisations: Organisation[]) => {
        this.organisationsService.setOrganisations(organisations);
      }
    );
  }


  loadCategories() {
    this.httpClient.get<Category[]>(environment.backendServer + '/categories/',
      {observe: 'body', responseType: 'json'}).pipe(map(
      (categories) => {
        console.log(categories);
        return categories;
      }
    )).subscribe(
      (categories: Category[]) => {
        this.categoryService.setCategories(categories);
      }
    );
  }

  loadExercisesByCategory(categoryId: string) {
    const url = environment.backendServer + '/categories/' + categoryId + '/exercises/';

    this.httpClient.get<Exercise[]>(url,
      {observe: 'body', responseType: 'json'}).pipe(map(
      (exercises) => {
        console.log(exercises);
        return exercises;
      }
    )).subscribe(
      (exercises: Exercise[]) => {
        this.exerciseService.setExercises(exercises);
      }
    );
  }

  getExercise(exerciseId: string): Observable<Exercise> {
    const url = environment.backendServer + '/categories/' + 'XXXXX' + '/exercises/' + exerciseId;
    return this.httpClient.get<Exercise>(url,
      {observe: 'body', responseType: 'json'});
  }

}
