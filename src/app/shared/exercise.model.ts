export class Exercise {
  public _id: string;
  public name: string;
  public youtubeVideoId: string;
  public title: string;
  public shortDescription: string;
  public description: string;
  public stepByStep: string;
  author: {id: string, username: string};

  constructor(_id: string, name: string, youtubeVideoId: string, title: string, shortDescription: string,
              description: string, stepByStep: string, author) {
    this._id = _id;
    this.name = name;
    this.youtubeVideoId = youtubeVideoId;
    this.title = title;
    this.shortDescription = shortDescription;
    this.description = description;
    this.stepByStep = stepByStep;
    this.author = author;
  }
}
