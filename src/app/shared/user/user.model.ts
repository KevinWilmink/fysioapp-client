export class User {
  public _id: string;
  public username: string;
  public password: string;
  public accountType: number;
  public userExercises;

  constructor(_id: string, username: string, password: string, accountType: number, userExercises) {
    this._id = _id;
    this.username = username;
    this.password = password;
    this.accountType = accountType;
    this.userExercises = userExercises;
  }
}
