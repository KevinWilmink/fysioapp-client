import { Injectable } from '@angular/core';
import {User} from './user.model';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class UserService {

  user: User;
  userChanged: Subject<User> = new Subject<User>();

  constructor() { }

  setUser(user: User) {
    this.user = user;
    this.userChanged.next(this.user);
  }

}
