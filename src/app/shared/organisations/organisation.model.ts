import {User} from '../user/user.model';

export class Organisation {
  public _id: string;
  public name: string;
  public logo: string;
  public users: User[];

  constructor(_id: string, name: string, logo: string, users: User[]) {
    this._id = _id;
    this.name = name;
    this.logo = logo;
    this.users = users;
  }
}
