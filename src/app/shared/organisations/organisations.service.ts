import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Organisation} from './organisation.model';

@Injectable()
export class OrganisationsService {

  // single organisation (details)
  organisation: Organisation;
  organisationChanged: Subject<Organisation> = new Subject<Organisation>();

  // mulitple organisations
  organisations: Organisation[];
  organisationsChanged: Subject<Organisation[]> = new Subject<Organisation[]>();

  constructor() { }


  setOrganisation(nwOrganisation: Organisation) {
    this.organisation = nwOrganisation;
    this.organisationChanged.next(this.organisation);
  }

  setOrganisations(nwOrganisations: Organisation[]) {
    this.organisations = nwOrganisations;
    this.organisationsChanged.next(this.organisations.slice());
  }



}
