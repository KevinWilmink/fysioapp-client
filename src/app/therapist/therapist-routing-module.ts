import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TherapistComponent} from './therapist/therapist.component';


const therapistRoutes: Routes = [
  {path: 'therapist', component: TherapistComponent},
]

@NgModule({
  imports: [
    RouterModule.forChild(therapistRoutes) /* ALTIJD forChild() als het niet de root is */
  ],
  exports: [RouterModule],
  providers: []
})

export class TherapistRoutingModule {}
