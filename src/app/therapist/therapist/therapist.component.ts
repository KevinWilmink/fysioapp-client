import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {DataStorageService} from '../../shared/data-storage.service';
import {Organisation} from '../../shared/organisations/organisation.model';
import {Subscription} from 'rxjs/Subscription';
import {OrganisationsService} from '../../shared/organisations/organisations.service';
import {UserService} from '../../shared/user/user.service';
import {User} from '../../shared/user/user.model';

@Component({
  selector: 'app-therapist',
  templateUrl: './therapist.component.html',
  styleUrls: ['./therapist.component.css']
})
export class TherapistComponent implements OnInit, OnDestroy {
  organisation: Organisation = null;
  private organisationSubscription: Subscription;

  private user: User;

  constructor(private dataStorageService: DataStorageService, private authService: AuthService,
              private organisationsService: OrganisationsService, private userService: UserService) {
    this.user = this.userService.user;

    this.organisationSubscription = this.organisationsService.organisationChanged.subscribe(
      (organisation: Organisation) => {
        this.organisation = organisation;
        console.log(this.organisation);
      }
    );
  }

  getUsersInOrganisation() {
    if (this.organisation) {
      if (this.organisation.users) {
        return this.organisation.users;
      }
    }
  }


  ngOnInit() {
    this.dataStorageService.loadOrganisationTherapist(this.user._id);
  }

  ngOnDestroy() {
    this.organisationSubscription.unsubscribe();
  }
}


