import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TherapistRoutingModule} from './therapist-routing-module';
import { TherapistComponent } from './therapist/therapist.component';
import { UserItemComponent } from './therapist/user-item/user-item.component';

@NgModule({
  imports: [
    CommonModule,
    TherapistRoutingModule
  ],
  declarations: [TherapistComponent, UserItemComponent]
})
export class TherapistModule { }
