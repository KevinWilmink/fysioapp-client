import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserExercisesComponent} from './user-exercises/user-exercises.component';

const clientRoutes: Routes = [
      {path: 'userExercises', component: UserExercisesComponent},
]

@NgModule({
  imports: [
    RouterModule.forChild(clientRoutes) /* ALTIJD forChild() als het niet de root is */
  ],
  exports: [RouterModule],
  providers: []
})

export class ClientRoutingModule {}
