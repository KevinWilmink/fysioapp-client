import {Component, OnDestroy, OnInit} from '@angular/core';
import {Category} from '../../overview/categories/category.model';
import {CategoriesService} from '../../overview/categories/categories.service';
import {Subscription} from 'rxjs/Subscription';
import {DataStorageService} from '../../shared/data-storage.service';

@Component({
  selector: 'app-user-exercises',
  templateUrl: './user-exercises.component.html',
  styleUrls: ['./user-exercises.component.css']
})
export class UserExercisesComponent implements OnInit, OnDestroy {
  private categoriesSubscription: Subscription;
  categories: Category[] = [];

  constructor(private categoryService: CategoriesService, private dataStorageService: DataStorageService) {
    this.categoriesSubscription = this.categoryService.categoriesChanged.subscribe(
      (categories: Category[]) => {
        this.categories = categories;
        console.log(this.categories);
      }
    );
  }

  ngOnInit() {
    // this.categories = this.categoryService.getCategories();
    this.dataStorageService.loadCategories();
  }

  ngOnDestroy() {
    this.categoriesSubscription.unsubscribe();
  }

}
