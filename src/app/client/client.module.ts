import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserExercisesComponent } from './user-exercises/user-exercises.component';
import {ClientRoutingModule} from './client-routing-module';

@NgModule({
  imports: [
    CommonModule,
    ClientRoutingModule
  ],
  declarations: [UserExercisesComponent]
})
export class ClientModule { }
