import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {OverviewModule} from './overview/overview.module';
import {AppRoutingModule} from './app-routing-module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {CategoriesService} from './overview/categories/categories.service';
import {DataStorageService} from './shared/data-storage.service';
import { LandingPageComponent } from './landing-page/landing-page.component';
import {ExerciseService} from './shared/exercise.service';
import { ExerciseComponent } from './shared/exercise/exercise.component';
import {ClientModule} from './client/client.module';
import {AuthModule} from './auth/auth.module';
import {AuthService} from './auth/auth.service';
import {AuthInterceptor} from './shared/interceptors/auth.interceptor';
import {OrganisationsService} from './shared/organisations/organisations.service';
import {TherapistModule} from './therapist/therapist.module';
import {UserService} from './shared/user/user.service';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    ExerciseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    OverviewModule,
    ClientModule,
    AuthModule,
    TherapistModule
  ],
  providers: [
    CategoriesService,
    DataStorageService,
    ExerciseService,
    AuthService,
    OrganisationsService,
    UserService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ], // TODO: move these to some kind of core-module
  bootstrap: [AppComponent]
})
export class AppModule { }
