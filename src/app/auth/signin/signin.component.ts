import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {HttpHeaders} from '@angular/common/http';
import {UserService} from '../../shared/user/user.service';
import {User} from '../../shared/user/user.model';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit, OnDestroy {
  message: string;

  user: User; // waarschijnlijk niet nodig: na inloggen komt result vd gebruiker terug. TODO: verwijder dit
  userSub: Subscription; // TODO: remove dit

  constructor(private authService: AuthService, private router: Router, private userService: UserService) {
    this.userSub = this.userService.userChanged.subscribe((nwUser: User) => {
      this.user = nwUser;
    });
  }

  ngOnInit() {
    console.log('DEBUG auto login as therapist');
    this.DEBUG_AutoLogin();
  }


  ngOnDestroy() {
    this.userSub.unsubscribe();
  }

  onSignin(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    this.authService.signinUser(email, password, (res) => {
          // login as client
        if (this.userService.user.accountType === 0) {
          this.router.navigate(['userExercises']);

          // login as therapist
        } else if (this.userService.user.accountType === 1) {
          this.router.navigate(['therapist']);

          // login as admin
        } else if (this.userService.user.accountType === 2) {
          this.router.navigate(['categories']);
        } else {
          console.log('Something went wrong, account is not specified with a accountType');
        }
      },
      (err) => {
        console.log('fout met inloggen');
        console.log(err);
        // this.router.navigate(['userExercises'], {});
        this.message = err;
      });
  }


  DEBUG_AutoLogin() {
    this.authService.signinUser('therapist@test.nl', 'testtest', (res) => {
      this.router.navigate(['therapist']);
    }, (err) => {
      console.log(err);
    });
  }

}
