import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
// import * as firebase from 'firebase';
import {catchError} from 'rxjs/operators';
import {HttpClient, HttpRequest} from '@angular/common/http';
// import 'firebase/auth';
import * as moment from 'moment';
import { environment } from '../../environments/environment';
import {UserService} from '../shared/user/user.service';


/* Authentication met Firebase
  - in firebase enable bij authentication password/email
  - $npm install firebase --save
  - initieer firebase app (zie app.component) met apiKey en authDomain
  - nu kan je inloggen met firebase.auth().signInWithEmailAndPassword etc. Token word opgeslagen in storage (zie hieronder)
  - zorg ervoor dat op firebase read/write op auth != null staan!
  - de token kan opgehaald worden op elk moment, maar dit is een async call, daarom is het een goed practise dit gelijk naar login te doen
    - daar stuurt GetToken() direct de oude token terug, maar async ververst hij de deze
  - calls naar de DB moet je nu de token aan mee sturen, dmv: baseUrl/tabelNaam.json?auth=tokenString

  Authentication in SPA:
    - gebruiker stuurt authentication informatie,
    - Server checked dit en stuurt token terug (containing: auth info)
    - Komende verzoeken sturen de token mee (voorkomt meerdere keren moeten inloggen)
    - Bij SPA (angular) is de server NIET op de hoogte van de clients (bij traditionele webapps WEL (via server: Session, client: Cookie)

  Token
  - de token word standaard opgeslagen in storage, in chrome te vinden via:
        Application > indexedDB> firebaseLocalStorageDB> firebaseLocalStorage (key)
      - anders: Application > Storage > Local Storage > localhost
 */





@Injectable()
export class AuthService {
  // fire.auth() > access to auth functions
  token: string;

  constructor(private router: Router, private httpClient: HttpClient, private userService: UserService) {}


  /* //backup
  signupUser(email: string, password: string) {
    const url = environment.backendServer + '/register';
    // returned een observable
    // return this.httpClient.request(req);
    console.log(email + ' < client > ' + password);
    return this.httpClient.post(url, {'username' : email, 'password' : password, 'organisationId' : 0}); // organisationId=0 > Client
  }*/

  // todo: signin after registering
  signupClient(email: string, password: string, organisationId: string,
             callbackSuccess: Function, callbackError: Function) {
    const url = environment.backendServer + '/register';
    this.httpClient.post(url,
      {'username' : email, 'password' : password, 'organisationId' : organisationId }
      ).subscribe(res => {
      this.token = res['token'];
      callbackSuccess(res);
    }, err => {
      callbackError(err);
    });
  }


  signupTherapist(email: string, password: string, organisationId: string,
                   callbackSuccess: Function, callbackError: Function) {
    const url = environment.backendServer + '/registerTherapist';
    this.httpClient.post(url,
      {'username' : email, 'password' : password, 'organisationId' : organisationId }
    ).subscribe(res => {
      this.token = res['token'];
      callbackSuccess(res);
    }, err => {
      callbackError(err);
    });
  }


  signupAdmin(email: string, password: string, adminPassword: string,
               callbackSuccess: Function, callbackError: Function) {
    const url = environment.backendServer + '/registerAdmin';
    this.httpClient.post(url,
      {'username' : email, 'password' : password, 'adminPassword': adminPassword}
    ).subscribe(res => {
      this.token = res['token'];
      callbackSuccess(res);
    }, err => {
      callbackError(err);
    });
  }


  singupBACKUP(email: string, password: string, organisationId: string, accountType: string, adminPassword: string,
              callbackSuccess: Function, callbackError: Function) {

    const url = environment.backendServer + '/register';

    // returned een observable
    // return this.httpClient.request(req);
    console.log(email + ' < client > ' + password);

    // organisationId=0 > Client
    this.httpClient.post(url,
      {'username' : email, 'password' : password, 'organisationId' : organisationId,
        'accountType': accountType, 'adminPassword': adminPassword}
    ).subscribe(res => {
      this.token = res['token'];
      callbackSuccess(res);
    }, err => {
      callbackError(err);
    });
  }


  signinUser(email: string, password: string, callbackSuccess: Function, callbackError: Function) {
    const url = environment.backendServer + '/login';
    this.httpClient.post(url, {'username' : email, 'password' : password}).subscribe(res => {
      // this.token = res['token'];
      // console.log('Login success: , token: ' + this.token);
      this.setSession(res);
      console.log('current token set: ' + localStorage.getItem('id_token'));
      callbackSuccess(res);
    }, err => {
      callbackError(err);
    });
  }


  private setSession(authResult) {
    const expiresAt = moment().add(authResult.expiresIn, 'second');
    console.log(authResult);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
    // this.loggedinUser =  authResult.user;

    // console.log('set user in setSession');

    this.userService.setUser(authResult.user);
    console.log('his.userService');
    console.log(this.userService.user);
  }


  logout() {
    /*
    firebase.auth().signOut();
    this.token = null;
    */
  }



  // get the token will return a promise. it will retrieve a token from the server if there is no token and it will be validaded
  getToken() {
    /*
    // haal opnieuw de token op en ververs hem, stuur echter de oude token terug.
    firebase.auth().currentUser.getIdToken().then(
      (token: string) => {
        this.token = token;
      });
    // Het kan voorkomen dat de oude token nu net experired is, vang dit op dmv error handling
    return this.token;
    */
    return this.token;
  }


  isAuthenticated() {
    return this.token != null;
  }
}
