import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../auth.service';
import {Organisation} from '../../shared/organisations/organisation.model';
import {Subscription} from 'rxjs/Subscription';
import {OrganisationsService} from '../../shared/organisations/organisations.service';
import {DataStorageService} from '../../shared/data-storage.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {
  organisations: Organisation[] = [];
  private organisationSubscription: Subscription;
  @ViewChild('signupForm') signupForm: NgForm;

  accountTypes: {id: number, name: string}[] = [{id: 0, name: 'client'}, {id: 1, name: 'therapist'}, {id: 2, name: 'admin'}];


  organisationIdSelected: string;
  accountTypeSelected = {id: 0, name: 'client'};

  constructor(private authService: AuthService, private organisationService: OrganisationsService,
              private dataStorage: DataStorageService) {
    this.organisationSubscription = this.organisationService.organisationsChanged.subscribe(
      (organisations: Organisation[]) => {
        this.organisations = organisations;
        if (this.organisations.length > 0) {

          // select default values
          this.organisationIdSelected = this.organisations[0]._id; // select the first element
          this.accountTypeSelected = this.accountTypes[0];
        }
      }
    );
  }


  ngOnDestroy() {
    this.organisationSubscription.unsubscribe();
  }


  ngOnInit() {
    this.dataStorage.loadOrganisations();
  }

  onSignup() {
    const email = this.signupForm.value.email;
    const password = this.signupForm.value.password;
    const organisationId = this.signupForm.value.organisationSelectedField;

    console.log(
      'email: ' + email +
      ' ,password: ' + password +
      ' ,organisationId: ' + organisationId +
      ' ,adminPassword:' + this.signupForm.value.adminPassword +
      ' ,accountType: ' + this.accountTypeSelected.id
    )

    // client
    if (this.accountTypeSelected.id === 0) {
      this.authService.signupClient(email, password, organisationId,
        this.signupSuccessfully, this.signupError);
    } else if (this.accountTypeSelected.id === 1) {
      this.authService.signupTherapist(email, password, organisationId,
        this.signupSuccessfully, this.signupError);
    } else if (this.accountTypeSelected.id === 2) {
      this.authService.signupAdmin(email, password, this.signupForm.value.adminPassword,
        this.signupSuccessfully, this.signupError);
    }



    /*
    this.authService.signupUser(
      email,
      password,
      this.signupForm.value.organisationSelectedField,
      this.signupForm.value.accountTypeSelectedField.id,
      this.signupForm.value.adminPassword,
      (res) => {
      console.log('successfully signed up!');
    }, (error) => {
      console.log('Could not signup');
      console.log(error);
    } );
    */

    /*
    this.authService.signupUser(email, password).subscribe(res => {
      console.log(res);
    }, err => {
      console.log('error occured');
    });
    */
  }

  signupSuccessfully(res) {
    console.log('successfully signed up!');
    // TODO: save token and continue in application (see login)
  }

  signupError(err) {
    console.log('error signing up!')
    console.log(err);
  }

}
