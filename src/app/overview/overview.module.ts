import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryComponent } from './categories/category/category.component';
import {OverviewRoutingModule} from './overview-routing-module';
import { CategoryItemComponent } from './categories/category-item/category-item.component';
import { ExerciseItemComponent } from './categories/category/exercise-item/exercise-item.component';

@NgModule({
  imports: [
    CommonModule,
    OverviewRoutingModule
  ],
  declarations: [
    CategoriesComponent,
    CategoryComponent,
    CategoryItemComponent,
    ExerciseItemComponent
  ]
})
export class OverviewModule { }
