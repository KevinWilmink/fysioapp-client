import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CategoriesComponent} from './categories/categories.component';
import {CategoryComponent} from './categories/category/category.component';

const overviewRoutes: Routes = [
      {path: 'categories', component: CategoriesComponent},
      {path: 'categories/:id', component: CategoryComponent },
]

@NgModule({
  imports: [
    RouterModule.forChild(overviewRoutes) /* ALTIJD forChild() als het niet de root is */
  ],
  exports: [RouterModule],
  providers: []
})

export class OverviewRoutingModule {}
