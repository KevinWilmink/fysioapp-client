import { Injectable } from '@angular/core';
import {Category} from './category.model';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class CategoriesService {

  categoryLastLoadedId = '';
  categoriesChanged: Subject<Category[]> = new Subject<Category[]>();

  private categories: Category[] = [
    new Category('11111111', 'Schouders', 'Hierin een overzicht van alle schouder oefeningen'),
    new Category('22222222', 'knie', 'Oefeningen voor de knie vind u hier'),
    new Category('33333333', 'Borst', 'Borst gerelateerde oefeningen vind u in deze category')
  ];

  constructor() { }

  getCategories() {
    return this.categories;
  }

  setCategories(categories: Category[]) {
    this.categories = categories;
    this.categoriesChanged.next(this.categories.slice());
  }

}
