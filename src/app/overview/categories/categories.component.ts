import {Component, OnDestroy, OnInit} from '@angular/core';
import {Category} from './category.model';
import {CategoriesService} from './categories.service';
import {Subscription} from 'rxjs/Subscription';
import {DataStorageService} from '../../shared/data-storage.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit, OnDestroy {

  categories: Category[] = [];
  private categoriesSubscription: Subscription;

  constructor(private categoryService: CategoriesService, private dataStorageService: DataStorageService) {
    this.categoriesSubscription = this.categoryService.categoriesChanged.subscribe(
      (categories: Category[]) => {
        this.categories = categories;
      }
    );
  }

  ngOnInit() {
     // this.categories = this.categoryService.getCategories();
    this.dataStorageService.loadCategories();
  }

  ngOnDestroy() {
    this.categoriesSubscription.unsubscribe();
  }
}
