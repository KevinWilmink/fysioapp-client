export class Category {
  public _id: string;
  public name: string;
  public description: string;

  constructor(_id: string, name: string, description: string) {
    this._id = _id;
    this.name = name;
    this.description = description;
  }
}
