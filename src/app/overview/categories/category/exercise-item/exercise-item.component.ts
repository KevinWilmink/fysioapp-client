import {Component, Input, OnInit} from '@angular/core';
import {Exercise} from '../../../../shared/exercise.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-exercise-item',
  templateUrl: './exercise-item.component.html',
  styleUrls: ['./exercise-item.component.css']
})
export class ExerciseItemComponent implements OnInit {
  @Input() exercise: Exercise;

  constructor(private router: Router) { }

  ngOnInit() {
  }


  onClickExercise() {
    const url = 'exercises/' + this.exercise._id;
    this.router.navigate([url]);
  }

}
