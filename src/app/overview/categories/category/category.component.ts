import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {ExerciseService} from '../../../shared/exercise.service';
import {DataStorageService} from '../../../shared/data-storage.service';
import {Exercise} from '../../../shared/exercise.model';
import {Subscription} from 'rxjs/Subscription';
import {CategoriesService} from '../categories.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit, OnDestroy {
  exercises: Exercise[];

  exerciseSub: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private exerciseService: ExerciseService, private dataStorage: DataStorageService,
              private categoriesService: CategoriesService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      (params: Params) => {
        const categoryId = params['id'];
        this.categoriesService.categoryLastLoadedId = categoryId;
        this.dataStorage.loadExercisesByCategory(categoryId);
      }
    );

    this.exerciseSub = this.exerciseService.exercisesChanged.subscribe(
      (exercises: Exercise[]) => {
        this.exercises = exercises;
      }
    );
  }

  ngOnDestroy() {
    this.exerciseSub.unsubscribe();
  }

}
