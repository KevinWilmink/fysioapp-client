import {Component, Input, OnInit} from '@angular/core';
import {Category} from '../category.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-category-item',
  templateUrl: './category-item.component.html',
  styleUrls: ['./category-item.component.css']
})
export class CategoryItemComponent implements OnInit {
  @Input() category: Category;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }


  onClickItem() {
    const nwUrl = this.category._id;
    this.router.navigate([nwUrl], {relativeTo: this.activatedRoute});
  }

}
