import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LandingPageComponent} from './landing-page/landing-page.component';
import {ExerciseComponent} from './shared/exercise/exercise.component';


const appRoutes: Routes = [
  {path: '', component: LandingPageComponent},
  {path: 'exercises/:id', component: ExerciseComponent},
]

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule] /* export de module, dit komt er binnen bij een andere module die deze module import */
})


export class AppRoutingModule { /* what is exported here, will be imported the moment this module is imported */
}
